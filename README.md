# dark-eisberg.tdesktop-theme

## Screenshot
 
 ![screenshot](screenshot.PNG)

## Desc~

 Based on iOS dark theme color scheme.

 https://t.me/themes/100
 
## Installation

 1. Download the latest release.
 2. Upload to any desktop telegram chat.
 3. Open file and Apply theme.